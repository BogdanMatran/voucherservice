package util;

import java.util.ArrayList;

/**
 * Created by bmatran on 6/29/2017.
 */
public class MerchantShoppingCarts {

        public Merchant merchant;
        public double total;
        public ArrayList<ShoppingCartItems> shoppingCartItems;

    public void setShoppingCartItems(ArrayList<ShoppingCartItems> shoppingCartItems) {
        this.shoppingCartItems = shoppingCartItems;
    }

    public ArrayList<ShoppingCartItems> getShoppingCartItems() {

        return shoppingCartItems;
    }

    public MerchantShoppingCarts(double total, Merchant merchant, ArrayList<ShoppingCartItems> shoppingCartItems) {

        this.total = total;
        this.merchant = merchant;
        this.shoppingCartItems = shoppingCartItems;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public double getTotal() {

        return total;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public MerchantShoppingCarts(double total, Merchant merchant) {

        this.total = total;
        this.merchant = merchant;
    }

    public MerchantShoppingCarts() {
    }
}
