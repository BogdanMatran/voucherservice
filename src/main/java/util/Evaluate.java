package util;

import java.util.ArrayList;

/**
 * Created by ts-bogdan.matran on 11/07/17.
 */
public class Evaluate {
    public ArrayList<Iteme> items ;

    public Evaluate(ArrayList<Iteme>items) {
        this.items = items;
    }

    public ArrayList<Iteme> getItems() {
        return items;
    }

    public void setItems(ArrayList<Iteme> items) {
        this.items = items;
    }

    public Evaluate() {
    }
}
