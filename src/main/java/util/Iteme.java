package util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ts-bogdan.matran on 11/07/17.
 */
public class Iteme {
    public int productId;
    public int shopId;
    public int merchantId;
    public int brandId;
    public ArrayList<Integer>shopCategories;
    public ArrayList<Integer> categories;
    public double price;
    public int quantity;
    public boolean fixedBookPriceBinding;


    public Iteme(int productId, int shopId, int merchantId, int brandId, ArrayList<Integer> shopCategories, ArrayList<Integer> categories, double price, int quantity, boolean fixedBookPriceBinding) {
        this.productId = productId;
        this.shopId = shopId;
        this.merchantId = merchantId;
        this.brandId = brandId;
        this.shopCategories = shopCategories;
        this.categories = categories;
        this.price = price;
        this.quantity = quantity;
        this.fixedBookPriceBinding = fixedBookPriceBinding;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public List<Integer> getShopCategories() {
        return shopCategories;
    }

    public void setShopCategories(ArrayList<Integer> shopCategories) {
        this.shopCategories = shopCategories;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Integer> categories) {
        this.categories = categories;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isFixedBookPriceBinding() {
        return fixedBookPriceBinding;
    }

    public void setFixedBookPriceBinding(boolean fixedBookPriceBinding) {
        this.fixedBookPriceBinding = fixedBookPriceBinding;
    }

    public Iteme() {
    }
}
