package util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import util.Merchant;
import util.MerchantShoppingCarts;
import util.ShoppingCartItems;
/**
 * Created by bmatran on 6/28/2017.
 */
public class CreateJson {
    private double total;
    private ArrayList<MerchantShoppingCarts> merchantShoppingCarts;

    public void setTotal(double total) {
        this.total = total;
    }

    public void setMerchantShoppingCarts(ArrayList<MerchantShoppingCarts> merchantShoppingCarts) {
        this.merchantShoppingCarts = merchantShoppingCarts;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setVisitorId(Integer visitorId) {
        this.visitorId = visitorId;
    }

    public void setDebitorId(String debitorId) {
        this.debitorId = debitorId;
    }

    public void setMallKey(String mallKey) {
        this.mallKey = mallKey;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public double getTotal() {

        return total;
    }

    public ArrayList<MerchantShoppingCarts> getMerchantShoppingCarts() {
        return merchantShoppingCarts;
    }


    public String getCampaignId() {
        return campaignId;
    }

    public Integer getVisitorId() {
        return visitorId;
    }

    public String getDebitorId() {
        return debitorId;
    }

    public String getMallKey() {
        return mallKey;
    }

    public String getCoupon() {
        return coupon;
    }

    public CreateJson(double total, ArrayList<MerchantShoppingCarts> merchantShoppingCarts, String campaignId, Integer visitorId, String debitorId, String mallKey, String coupon) {
        this.total = total;
        this.merchantShoppingCarts = merchantShoppingCarts;
        this.campaignId = campaignId;
        this.visitorId = visitorId;
        this.debitorId = debitorId;
        this.mallKey = mallKey;
        this.coupon = coupon;
    }

    public CreateJson() {

    }

    private String campaignId;
    private Integer visitorId;
    private String debitorId;
    private String mallKey;
    private String coupon;
}
