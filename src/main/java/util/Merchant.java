package util;

/**
 * Created by bmatran on 6/29/2017.
 */
public class Merchant {
    public String merchantId;

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantId() {

        return merchantId;
    }

    public Merchant() {

    }

    public Merchant(String merchantId) {

        this.merchantId = merchantId;
    }
}
