package util;

import java.util.ArrayList;

/**
 * Created by bmatran on 6/29/2017.
 */
public class ShoppingCartItems {
    private String productId ;
    private String shopId;
    private int brandId;
    private ArrayList<String> categories;
    private double price;
    private Integer quantity;
    private boolean fixedBookPriceBinding;

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setFixedBookPriceBinding(boolean fixedBookPriceBinding) {
        this.fixedBookPriceBinding = fixedBookPriceBinding;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
    public String getProductId() {

        return productId;
    }

    public String getShopId() {
        return shopId;
    }

    public double getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public boolean isFixedBookPriceBinding() {
        return fixedBookPriceBinding;
    }

    public ShoppingCartItems(String productId, String shopId, int brandId,double price, Integer quantity, boolean fixedBookPriceBinding) {
        super();
        this.productId = productId;
        this.shopId = shopId;
        this.brandId=brandId;
        this.price = price;
        this.quantity = quantity;
        this.fixedBookPriceBinding = fixedBookPriceBinding;
    }

    public ShoppingCartItems() {
    }
}
