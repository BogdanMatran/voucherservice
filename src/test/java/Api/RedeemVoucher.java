package Api;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;


/**
 * Created by ts-bogdan.matran on 27/06/17.
 */

//redeem for single product all type off coupons
public class RedeemVoucher extends ArraysCreation {
    @Test
    public void MerchantCouponSingleProduct () throws IOException{
        InitializeArrays();
        shoppingCartItems.setProductId("670490921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("56140");
        merchantShoppingCarts.setTotal(31.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(31.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("56vxdg38");
        Assert.assertEquals(PostMethod().getStatusCode(),200,"Status 200 : post corect !");
        String responseBody = PostMethod().getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(total,11.00);
        Assert.assertEquals(discount,20.00);

    }
@Test
    public void ProductCouponSingleProduct() throws JsonProcessingException{
    InitializeArrays();
    shoppingCartItems.setProductId("670490921");
    shoppingCartItems.setShopId("53425");
    shoppingCartItems.setPrice(31.00);
    shoppingCartItems.setQuantity(1);
    shoppingCartItems.setFixedBookPriceBinding(false);
    arrayOfShoppingCartItems.add(shoppingCartItems);
    merchants.setMerchantId("56140");
    merchantShoppingCarts.setTotal(321.00);
    merchantShoppingCarts.setMerchant(merchants);
    merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
    arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
    jsonobj.setTotal(321.00);
    jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
    jsonobj.setCampaignId("");
    jsonobj.setVisitorId(0);
    jsonobj.setDebitorId("16977409");
    jsonobj.setMallKey("de");
    jsonobj.setCoupon("f6yumee");
    String responseBody = PostMethod().getBody().asString();
    JsonPath jsonPath = new JsonPath(responseBody);
    Assert.assertEquals(PostMethod().getStatusCode(),200,"Status 200 : post corect !");
    double total= jsonPath.getDouble("total");
    Double discount = jsonPath.getDouble("couponDiscountTotal");
    Assert.assertEquals(total,306.00);
    Assert.assertEquals(discount,15.00);
}
    @Test
    public void BrandCouponSingleProduct()throws JsonProcessingException{
      InitializeArrays();
        shoppingCartItems.setProductId("670490921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setBrandId(2);
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("56140");
        merchantShoppingCarts.setTotal(321.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(321.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("uj94j34v");
        String responseBody = PostMethod().getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(PostMethod().getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,311.00);
        Assert.assertEquals(discount,10.00);
    }
    @Test
    public void CategoriesCouponSingleProduct() throws JsonProcessingException{
        InitializeArrays();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        shoppingCartItems.setProductId("670490921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setBrandId(2);
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setCategories(categories);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("56140");
        merchantShoppingCarts.setTotal(321.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(321.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("87kd2w8r");
        String responseBody = PostMethod().getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(PostMethod().getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,301.00,"Total amount to pay!");
        Assert.assertEquals(discount,20.00,"Discount offered using coupon!");
    }
    @Test
    public void MerchantCouponInvalid() throws JsonProcessingException{
        InitializeArrays();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        shoppingCartItems.setProductId("670490921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setBrandId(2);
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setCategories(categories);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("561");
        merchantShoppingCarts.setTotal(321.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(321.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("56vxdg38");
        String responseBody = PostMethod().getBody().asString();
        System.out.println(responseBody);
        JsonPath jsonPath = new JsonPath(responseBody);
        Boolean errorMessage = jsonPath.getBoolean("success");
        Assert.assertEquals(PostMethod().getStatusCode(),406,"Status 400 : Error !");
        Assert.assertFalse(errorMessage);
    }
    @Test
    public void ProductCouponInvalid() throws  JsonProcessingException{
        InitializeArrays();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        shoppingCartItems.setProductId("6704921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setBrandId(2);
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setCategories(categories);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("561");
        merchantShoppingCarts.setTotal(321.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(321.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("f6yumee");
        String responseBody = PostMethod().getBody().asString();
        System.out.println(responseBody);
        JsonPath jsonPath = new JsonPath(responseBody);
        Boolean errorMessage = jsonPath.getBoolean("success");
        Assert.assertEquals(PostMethod().getStatusCode(),406,"Status 400 : Error !");
        Assert.assertFalse(errorMessage);
    }
    @Test
    public void BrandCouponInvalid() throws JsonProcessingException{
        InitializeArrays();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        shoppingCartItems.setProductId("6704921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setBrandId(23);
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setCategories(categories);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("561");
        merchantShoppingCarts.setTotal(321.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(321.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("uj94j34v");
        String responseBody = PostMethod().getBody().asString();
        System.out.println(responseBody);
        JsonPath jsonPath = new JsonPath(responseBody);
        Boolean errorMessage = jsonPath.getBoolean("success");
        Assert.assertEquals(PostMethod().getStatusCode(),406,"Status 400 : Error !");
        Assert.assertFalse(errorMessage);
    }
    @Test
    public void CategoriesCouponInvalid() throws JsonProcessingException{
        InitializeArrays();
        categories.add(0,"12");
        categories.add(1,"BABY & KIND");
        shoppingCartItems.setProductId("6704921");
        shoppingCartItems.setShopId("53425");
        shoppingCartItems.setBrandId(23);
        shoppingCartItems.setPrice(31.00);
        shoppingCartItems.setCategories(categories);
        shoppingCartItems.setQuantity(1);
        shoppingCartItems.setFixedBookPriceBinding(false);
        arrayOfShoppingCartItems.add(shoppingCartItems);
        merchants.setMerchantId("561");
        merchantShoppingCarts.setTotal(321.00);
        merchantShoppingCarts.setMerchant(merchants);
        merchantShoppingCarts.setShoppingCartItems(arrayOfShoppingCartItems);
        arrayOfMerchantShoppingCarts.add(merchantShoppingCarts);
        jsonobj.setTotal(321.00);
        jsonobj.setMerchantShoppingCarts(arrayOfMerchantShoppingCarts);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("87kd2w8r");
        String responseBody = PostMethod().getBody().asString();
        System.out.println(responseBody);
        JsonPath jsonPath = new JsonPath(responseBody);
        Boolean errorMessage = jsonPath.getBoolean("success");
        Assert.assertEquals(PostMethod().getStatusCode(),406,"Status 400 : Error !");
        Assert.assertFalse(errorMessage);
    }
}