package Api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import util.Evaluate;
import util.Iteme;

import java.util.*;

import static io.restassured.RestAssured.given;


/**
 * Created by ts-bogdan.matran on 11/07/17.
 */
public class TestEvaluate {
@Test
   public void TestEval() throws JsonProcessingException{
       Evaluate jsonobj = new Evaluate();
       Iteme it = new Iteme();
       ArrayList<Iteme> item = new ArrayList<Iteme>();
       ArrayList<Integer> categ = new ArrayList<Integer>();
       ArrayList<Integer> rakCateg= new ArrayList<Integer>();
       categ.add(0,1);
       categ.add(1,5);
       categ.add(2,15);
       rakCateg.add(15);
       rakCateg.add(150);
       rakCateg.add(1500);
       it.setProductId(123119917);
       it.setShopId(0);
       it.setMerchantId(99726);
       it.setBrandId(0);
       it.setShopCategories(categ);
       it.setCategories(rakCateg);
       it.setPrice(150.00);
       it.setQuantity(2);
       it.setFixedBookPriceBinding(false);
       item.add(it);
       jsonobj.setItems(item);
    ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
    String jsonString = mapper.writeValueAsString(jsonobj);
    System.out.println(jsonString);
    Response r = given()
            .contentType("application/json")
            .body(jsonString)
            .when()
            .post("http://couponservice-staging.rakuten.de/evaluate/pdv");
    String responseBody = r.getBody().asString();
    JsonPath jsonPath = new JsonPath(responseBody);
    Boolean errorMessage = jsonPath.getBoolean("success");
    System.out.println(responseBody);
    System.out.println(r.statusCode());
    Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
    Assert.assertTrue(errorMessage);

}
@Test
    public void BrandProductMerchant() throws JsonProcessingException{
    Evaluate jsonobj = new Evaluate();
    Iteme it = new Iteme();
    ArrayList<Iteme> item = new ArrayList<Iteme>();
    ArrayList<Integer> categ = new ArrayList<Integer>();
    ArrayList<Integer> rakCateg= new ArrayList<Integer>();
    categ.add(0,15);
    categ.add(1,150);
    rakCateg.add(2);
    rakCateg.add(0);
    rakCateg.add(0);
    rakCateg.add(0);
    it.setProductId(655123);
    it.setShopId(0);
    it.setMerchantId(999);
    it.setBrandId(9);
    it.setShopCategories(categ);
    it.setCategories(rakCateg);
    it.setPrice(150.00);
    it.setQuantity(1);
    it.setFixedBookPriceBinding(false);
    item.add(it);
    jsonobj.setItems(item);
    ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
    String jsonString = mapper.writeValueAsString(jsonobj);
    System.out.println(jsonString);
    Response r = given()
            .contentType("application/json")
            .body(jsonString)
            .when()
            .post("http://couponservice-staging.rakuten.de/evaluate/pdv");
    String responseBody = r.getBody().asString();
    JsonPath jsonPath = new JsonPath(responseBody);
    System.out.println(responseBody);
    JSONObject obj = new JSONObject(responseBody);
    JSONArray arr = obj.getJSONArray("coupons");
    JSONObject brandJSONObject = arr.getJSONObject(0);
    String codeBrand = brandJSONObject.getString("code");
    JSONObject handlerJSONObject = arr.getJSONObject(1);
    String codeHandler = handlerJSONObject.getString("code");
    JSONObject produckteJSONObject = arr.getJSONObject(2);
    String codeProdukte = produckteJSONObject.getString("code");
    String descBrand = brandJSONObject.getString("discountDescription");
    String descHandler = handlerJSONObject.getString("discountDescription");
    String descProduckte = produckteJSONObject.getString("discountDescription");
    Boolean errorMessage = jsonPath.getBoolean("success");
    Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
    Assert.assertEquals(codeBrand,"2HCZ4C");
    Assert.assertEquals(descBrand,"10 € Rabatt auf ausgewählte Marken");
    Assert.assertEquals(codeHandler,"H6ZZX8");
    Assert.assertEquals(descHandler,"20 € Rabatt auf ausgewählte Händler");
    Assert.assertEquals(codeProdukte,"ZS6HU8");
    Assert.assertEquals(descProduckte,"20 € Rabatt pro Produkt auf ausgewählte Produkte");
    System.out.println(responseBody);
    System.out.println(r.statusCode());
    Assert.assertTrue(errorMessage);
}
    @Test
    public void ProductMerchant() throws JsonProcessingException{
        Evaluate jsonobj = new Evaluate();
        Iteme it = new Iteme();
        ArrayList<Iteme> item = new ArrayList<Iteme>();
        ArrayList<Integer> categ = new ArrayList<Integer>();
        ArrayList<Integer> rakCateg= new ArrayList<Integer>();
        categ.add(0,15);
        categ.add(1,150);
        categ.add(1500);
        rakCateg.add(15);
        rakCateg.add(150);
        rakCateg.add(1500);
        it.setProductId(123119917);
        it.setShopId(0);
        it.setMerchantId(99726);
        it.setBrandId(0);
        it.setShopCategories(categ);
        it.setCategories(rakCateg);
        it.setPrice(150.00);
        it.setQuantity(2);
        it.setFixedBookPriceBinding(false);
        item.add(it);
        jsonobj.setItems(item);
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        System.out.println(jsonString);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/evaluate/pdv");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        System.out.println(responseBody);
        JSONObject obj = new JSONObject(responseBody);
        JSONArray arr = obj.getJSONArray("coupons");
        JSONObject productkeMerchantJSONObject = arr.getJSONObject(0);
        String codeProdukteMerchant = productkeMerchantJSONObject.getString("code");
        JSONObject produckteJSONObject = arr.getJSONObject(1);
        String codeProdukte = produckteJSONObject.getString("code");
        JSONObject handlerJSONObject = arr.getJSONObject(2);
        String codeHandler = handlerJSONObject.getString("code");
        String descProduckteMerchant = productkeMerchantJSONObject.getString("discountDescription");
        String descHandler = handlerJSONObject.getString("discountDescription");
        String descProduckte = produckteJSONObject.getString("discountDescription");
        Boolean errorMessage = jsonPath.getBoolean("success");
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(codeProdukteMerchant,"PHSDTE");
        Assert.assertEquals(descProduckteMerchant,"30 € Rabatt pro Produkt auf ausgewählte Produkte und Händler");
        Assert.assertEquals(codeProdukte,"TSM92D");
        Assert.assertEquals(descProduckte,"21 € Rabatt pro Produkt auf ausgewählte Produkte");
        Assert.assertEquals(codeHandler,"XFZG85");
        Assert.assertEquals(descHandler,"20 € Rabatt auf ausgewählte Händler");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertTrue(errorMessage);
    }
    @Test
    public void rakutenCategories () throws JsonProcessingException{

        Evaluate jsonobj = new Evaluate();
        Iteme it = new Iteme();
        ArrayList<Iteme> item = new ArrayList<Iteme>();
        ArrayList<Integer> categ = new ArrayList<Integer>();
        ArrayList<Integer> rakCateg= new ArrayList<Integer>();
        categ.add(0,15);
        categ.add(1,150);
        categ.add(1500);
        rakCateg.add(15);
        rakCateg.add(160);
        rakCateg.add(1500);
        it.setProductId(1239917);
        it.setShopId(0);
        it.setMerchantId(726);
        it.setBrandId(0);
        it.setShopCategories(categ);
        it.setCategories(rakCateg);
        it.setPrice(150.00);
        it.setQuantity(2);
        it.setFixedBookPriceBinding(false);
        item.add(it);
        jsonobj.setItems(item);
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        System.out.println(jsonString);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/evaluate/pdv");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        System.out.println(responseBody);
        JSONObject obj = new JSONObject(responseBody);
        JSONArray arr = obj.getJSONArray("coupons");
        JSONObject rakutenCategoryJSONObject = arr.getJSONObject(0);
        String codeRakutenCategory = rakutenCategoryJSONObject.getString("code");
        Boolean success = jsonPath.getBoolean("success");
        Assert.assertEquals(codeRakutenCategory,"labeltest");

        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertTrue(success);

    }

}
