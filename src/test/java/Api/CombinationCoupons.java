package Api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import util.CreateJson;
import util.Merchant;
import util.MerchantShoppingCarts;
import util.ShoppingCartItems;

import java.io.IOException;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;

/**
 * Created by bmatran on 7/3/2017.
 */
public class CombinationCoupons {

    @Test
    public void ProductMerchantCoupon()throws JsonProcessingException, IOException{

        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("670490921");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("x4mfaeseb");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,50.00);
        Assert.assertEquals(discount,10.00);
    }
    @Test
    public void ProductMerchantBrand() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("670490921");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("gm64xbnd");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void ProductMerchantBrandCategories()throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("670490921");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setCategories(categories);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("z3m8z71");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,45.00);
        Assert.assertEquals(discount,15.00);
    }
    @Test
    public void ProductBrandCoupon() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("670490921");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setCategories(categories);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("azq2b3q");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,30.00);
        Assert.assertEquals(discount,30.00);

    }
    @Test
public void ProductCategoriesCoupon()throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("6704");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setCategories(categories);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("q27ujst");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void MerchantBrand() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("6704");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setCategories(categories);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("w8jym26kt");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void MerchantCategories()throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("6704");
        spp2.setShopId("53425");
        spp2.setBrandId(2);
        spp2.setCategories(categories);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("pypeg9g");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void BrandCategories() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        MerchantShoppingCarts non2 = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ArrayList<ShoppingCartItems> shp2 = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        ShoppingCartItems spp2 = new ShoppingCartItems();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        Merchant mer = new Merchant();
        Merchant mer2 = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(2);
        spp.setPrice(40.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        spp2.setProductId("6704");
        spp2.setShopId("53425");
        spp2.setBrandId(7);
        spp2.setCategories(categories);
        spp2.setPrice(20.00);
        spp2.setQuantity(1);
        spp2.setFixedBookPriceBinding(false);
        shp.add(spp);
        shp2.add(spp2);
        mer.setMerchantId("2");
        mer2.setMerchantId("21");
        non.setTotal(40.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        non2.setTotal(20.00);
        non2.setMerchant(mer2);
        non2.setShoppingCartItems(shp2);
        ms.add(non);
        ms.add(non2);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("bvr22zwf");
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(System.out, jsonobj);
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
}
