package Api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import util.CreateJson;
import util.Merchant;
import util.MerchantShoppingCarts;
import util.ShoppingCartItems;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;

/**
 * Created by ts-bogdan.matran on 17/07/17.
 */
public abstract class ArraysCreation {
    public CreateJson jsonobj;
    ArrayList<MerchantShoppingCarts> arrayOfMerchantShoppingCarts;
    MerchantShoppingCarts merchantShoppingCarts;
    ArrayList<ShoppingCartItems> arrayOfShoppingCartItems;
    ShoppingCartItems shoppingCartItems;
    Merchant merchants;
    ArrayList<String>categories ;
    public void InitializeArrays(){
        jsonobj = new CreateJson();
        arrayOfMerchantShoppingCarts = new ArrayList<MerchantShoppingCarts>(10);
        merchantShoppingCarts= new MerchantShoppingCarts();
        arrayOfShoppingCartItems = new ArrayList<ShoppingCartItems>(10);
        shoppingCartItems = new ShoppingCartItems();
        merchants = new Merchant();
        categories = new ArrayList<String>();
    }
    public Response PostMethod()throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response response = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        return response;
    }
}
