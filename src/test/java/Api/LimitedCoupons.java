package Api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import util.CreateJson;
import util.Merchant;
import util.MerchantShoppingCarts;
import util.ShoppingCartItems;

import javax.swing.plaf.PanelUI;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;

/**
 * Created by ts-bogdan.matran on 10/07/17.
 */
public class LimitedCoupons {


    @Test
    public void ProductLimited()throws JsonProcessingException
    {
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setPrice(42.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(42.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(42.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("vqcjzs2hr");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,22.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void ProductLimitedInvalid() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setPrice(21.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(21.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(21.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("vqcjzs2hr");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        Assert.assertEquals(r.getStatusCode(),406,"Status 400: invalid !");
        Boolean succ = jsonPath.getBoolean("success");
        String error =jsonPath.getString("error");
        Assert.assertEquals(error,"ORDER_VALUE_TOO_LOW");
        Assert.assertFalse(succ);

    }
    @Test
    public void MerchantLimited()throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setPrice(70.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("2");
        non.setTotal(70.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(70.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("1w6ngam");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,50.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void MerchantLimitedInvalid()throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("2");
        spp.setPrice(21.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(21.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(21.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("1w6ngam");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        Assert.assertEquals(r.getStatusCode(),406,"Status 400: invalid !");
        Boolean succ = jsonPath.getBoolean("success");
        String error =jsonPath.getString("error");
        Assert.assertEquals(error,"ORDER_VALUE_TOO_LOW");
        Assert.assertFalse(succ);
    }
    @Test
    public void BrandLimited() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(44631);
        spp.setPrice(60.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(60.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("3vyhj11nm");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void BrandLimitedInvalid()throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        spp.setProductId("670490921");
        spp.setShopId("2");
        spp.setBrandId(44631);
        spp.setPrice(21.00);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(21.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(21.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("3vyhj11nm");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        Assert.assertEquals(r.getStatusCode(),406,"Status 400: invalid !");
        Boolean succ = jsonPath.getBoolean("success");
        String error =jsonPath.getString("error");
        Assert.assertEquals(error,"ORDER_VALUE_TOO_LOW");
        Assert.assertFalse(succ);
    }
    @Test
    public void CategoriesLimit() throws JsonProcessingException{
        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        spp.setProductId("670490921");
        spp.setShopId("53425");
        spp.setBrandId(44631);
        spp.setPrice(60.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(60.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(60.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("cts7fxae7");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        double total= jsonPath.getDouble("total");
        Double discount = jsonPath.getDouble("couponDiscountTotal");
        Assert.assertEquals(r.getStatusCode(),200,"Status 200 : post corect !");
        Assert.assertEquals(total,40.00);
        Assert.assertEquals(discount,20.00);
    }
    @Test
    public void CategoriesLimitInvalid() throws  JsonProcessingException {

        CreateJson jsonobj = new CreateJson();
        ArrayList<MerchantShoppingCarts> ms = new ArrayList<MerchantShoppingCarts>(10);
        MerchantShoppingCarts non = new MerchantShoppingCarts();
        ArrayList<ShoppingCartItems> shp = new ArrayList<ShoppingCartItems>(10);
        ShoppingCartItems spp = new ShoppingCartItems();
        Merchant mer = new Merchant();
        ArrayList<String>categories = new ArrayList<String>();
        categories.add(0,"1");
        categories.add(1,"BABY & KIND");
        spp.setProductId("670490921");
        spp.setShopId("2");
        spp.setBrandId(44631);
        spp.setPrice(21.00);
        spp.setCategories(categories);
        spp.setQuantity(1);
        spp.setFixedBookPriceBinding(false);
        shp.add(spp);
        mer.setMerchantId("56140");
        non.setTotal(21.00);
        non.setMerchant(mer);
        non.setShoppingCartItems(shp);
        ms.add(non);
        jsonobj.setTotal(21.00);
        jsonobj.setMerchantShoppingCarts(ms);
        jsonobj.setCampaignId("");
        jsonobj.setVisitorId(0);
        jsonobj.setDebitorId("16977409");
        jsonobj.setMallKey("de");
        jsonobj.setCoupon("cts7fxae7");
        ObjectMapper mapper = new ObjectMapper();
        /*mapper.writeValue(System.out, jsonobj);*/
        String jsonString = mapper.writeValueAsString(jsonobj);
        Response r = given()
                .contentType("application/json")
                .body(jsonString)
                .when()
                .post("http://couponservice-staging.rakuten.de/redeem");
        String responseBody = r.getBody().asString();
        System.out.println(responseBody);
        System.out.println(r.statusCode());
        JsonPath jsonPath = new JsonPath(responseBody);
        Assert.assertEquals(r.getStatusCode(),406,"Status 400: invalid !");
        Boolean succ = jsonPath.getBoolean("success");
        String error =jsonPath.getString("error");
        Assert.assertEquals(error,"ORDER_VALUE_TOO_LOW");
        Assert.assertFalse(succ);
    }

}
